# teravin-video-kyc

![alt text](http://www.teravintech.com/assets/images/logo_teravin.png)

Teravin Liveness component for React.

## Installation

Add `teravin-video-kyc` as one of your dependenices in `package.json`

```json
{
  "name": "your-application-name",
  "version": "0.1.0",
  "private": true,
  "dependencies": {
    "@testing-library/jest-dom": "^5.11.4",
    "@testing-library/react": "^11.1.0",
    "@testing-library/user-event": "^12.1.10",",
    "react": "^17.0.2",
    "react-dom": "^17.0.2",
    "react-scripts": "4.0.3",
    "teravin-video-kyc": "https://aldipee@bitbucket.org/aldipee/teravin-video-kyc.git"
  },
```

Then run this command

```sh
// with npm
npm install

// with yarn
yarn install
```

## Demo

https://teravin-webrtc.web.app/video/call

## Usage

```jsx
import React from "react";
import {Video} from "teravin-video-kyc";

const initialData = {
    customerName: "Aldi Pranata"
    customerId: 'BC-22219922'
}

const resultDataLiveness = (data) => {
    console.log(data) // Data liveness when all process are finish
}

const LiveNessComponent = () => <Video initialData={initialData} onResult={resultDataLiveness} />;
```

## Lifecycles

There are some lifecycles on this library. Each step has the own cycle status. Every single time the lifecycles changes, the function that you passed on `onChangeStatus` props will be trigger. It is important for you to always give your own custom function on `onChangeStatus` props, so you have capability to handle all of these lifecycles.

| Status                         | Notes                                                                                                                                                                                                                                                                                                 |
| ------------------------------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `INITIAL_CONNECT`              | When library first trigger to start, will check connection to Teravin Server                                                                                                                                                                                                                          |
| `NOT_CONNECTED`                | If the library unable to conenect Liveness Server, then the status of the cycle will be `NOT_CONNECTED`                                                                                                                                                                                               |
| `CHECKING_INTERNET_CONNECTION` | After `INITIAL_CONNECT` succeed the library will check user Internet connection. At this phase, the status will be `CHECKING_INTERNET_CONNECTION`                                                                                                                                                     |
| `CONNECTION_UNSTABLE`          | After `CHECKING_INTERNET_CONNECTION`, this library will return status `CONNECTION_UNSTABLE` if user internet connection under 20ms. And at the same time the function you passed on `onError` props will be trigger. At this stage, you have to call method `Start()` to start the cycle from scracth |
| `RECORDING`                    | After `CHECKING_INTERNET_CONNECTION`, this library will return status `RECORDING` if user internet connection is stable. At this stage, the library will start recording. The recording will take 15 seconds, after that the camera will be close automatically                                       |
| `GENERATING_DATA`              | After `RECORDING`, this library will return status `GENERATING_DATA` . At this phase, the library will generating liveness data. We recommended you using this `GENERATING_DATA` cycle status to display loading indicator on your application.                                                       |
| `DONE`                         | After `GENERATING_DATA` done, the cycle will countinue to the end of the lifecycle which is `DONE`. At this stage this library will return liveness data. At the same time the library will trigger the function you passed on `onResult` props.                                                      |

### Method and Props

#### Props

The props here are specific to this component but one can pass any prop to the underlying video tag eg `className` or `style`

| prop           | type     | status      | notes                                                                                                 |
| -------------- | -------- | ----------- | ----------------------------------------------------------------------------------------------------- |
| initialName    | object   | `mandatory` | Custom field data to idendtify user.                                                                  |
| mediaConfig    | object   | `optional`  | MediaStream(s) for the video and audio                                                                |
| onRecording    | function | `mandatory` | Function for handler when the app starting to record                                                  |
| onResult       | function | `mandatory` | This function will trigger when all process is finish, and will return all data from liveness process |
| onStatusChange | function | `mandatory` | This function will be trigger when every time status change                                           |
| onError        | function | `mandatory` | min height of screenshot                                                                              |

#### Method

| method          | parameter | notes                                                |
| --------------- | --------- | ---------------------------------------------------- |
| Init()          | -         | Initialize liveness activity, will return tkyc token |
| StartLiveness() | -         | Start liveness activity                              |

## Methods Example

### `StartLiveness()`

This is sample how to use method `Start()` to start liveness activity

```jsx
import React from 'react';
import {Video} from 'teravin-video-kyc';

class YourAppComponent Extends React {
    constructor(props){
      super(props)
      this.state = {
          initialData: {
              name: "Customer Name"
          }
        }
      this.liveness = null
    }


    componentDidMount() {
    }

    handleClickButton = () => {
        this.liveness.current.StartLiveness()
    }
    handleInit = () => {
        this.liveness.current.Init()
    }

    render(){
        return (
            <div>
            <Video ref={(ref) => this.liveness} initialData={this.state.initialData} onResult={this.resultDataLiveness} />
             <button onClick={this.handleInit}>Initialize </button>
            <button onClick={this.handleClickButton}>Start Now </button>
            </div>
        )
    }

}
```

## Full Usage

This is sample of full usage of this library

```jsx
import React, { useRef } from 'react';
import { Video } from 'teravin-video-kyc';

function YourAppComponent() {
  const videoRef = useRef();

  useEffect(() => {
    (async () => {
      //  videoRef.current.Init() is initialize method that you have to call before StartLiveness()
      // This initialize method will return data contains tkyc token
      const data = await videoRef.current.Init();
      console.log(data, 'init');
    })();
  }, []);

  const handleOnRecord = (data) => {
    console.log(data, 'RECORDING_START');
  };

  const handleStatusChange = (data) => {
    console.log(data, 'STATUS_CHANGE');
  };

  const handleResult = (data) => {
    console.log(data, 'RESULT_LIVENESS');
  };

  const handleError = (error) => {
    console.log(error, 'ERROR_MESSAGE');
  };

  const handleClick = () => {
    videoRef.current.StartLiveness();
  };
  return (
    <div className='App'>
      <button onClick={handleClick}>Start</button>
      <Video
        ref={videoRef}
        onRecording={handleOnRecord}
        onResult={handleResult}
        onStatusChange={handleStatusChange}
        onError={handleError}
      />
    </div>
  );
}

export default YourAppComponent;
```

## License

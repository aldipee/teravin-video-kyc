import React, { useRef } from 'react';
import Video from '../src/components/Video';
function App() {
  const videoRef = useRef();
  const handleOnRecord = (data) => {
    console.log(data, 'RECORDING_START');
  };

  const handleStatusChange = (data) => {
    console.log(data, 'STATUS_CHANGE');
  };

  const handleResult = (data) => {
    console.log(data, 'RESULT_LIVENESS');
  };

  const handleError = (error) => {
    console.log(error, 'ERROR_MESSAGE');
  };

  const handleClick = () => {
    videoRef.current.Start();
  };
  return (
    <div className='App'>
      <button onClick={handleClick}>Start</button>
      <Video
        ref={videoRef}
        onRecording={handleOnRecord}
        onResult={handleResult}
        onStatusChange={handleStatusChange}
        onError={handleError}
      />
    </div>
  );
}

export default App;

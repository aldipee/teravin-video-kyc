import React from 'react';
import io from 'socket.io-client';
import PropTypes from 'prop-types';
import Connection from '../../lib/connection';
import jitterTime from 'jitter-time';
import { CONNECTION_STATUS, SOCKET_LISTENERS, SOCKET_EMITTER, CONNECTION_MESSAGE } from '../../config/constants';
import { UserMediaConfig } from '../../config/media';
import EventManager from '../../lib/eventManager';

class Video extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      status: CONNECTION_STATUS.OFFLINE,
      videoData: {},
      callStatus: '',
      kycToken: '',
    };
    this.socket = io(`https://liveness.colekbayar.com?room=customer`, {
      transports: ['websocket'],
    });
    this.recordingConnection = new Connection(this.socket);
    this.peerConnection = new Connection(this.socket);
    this.video = null;
  }

  Init = () => {
    const { onStatusChange } = this.props;
    this.socket.emit(SOCKET_EMITTER.GET_KYC_TOKEN);
    onStatusChange({ status: CONNECTION_STATUS.INITIAL });
    this.setState({ status: CONNECTION_STATUS.INITIAL });
    return new Promise((resolve, reject) => {
      this.socket.on(SOCKET_LISTENERS.GET_KYC_TOKEN, (data) => {
        this.setState({ kycToken: data.callId });
        resolve({ kycToken: this.state.kycToken });
      });
    });
  };

  StartLiveness = () => {
    const { initialData, onError, tkycToken = null } = this.props;
    const { status } = this.state;
    const data = { callId: tkycToken ? tkycToken : this.state.kycToken, ...initialData };

    if (status !== CONNECTION_STATUS.CONNECTION_UNSTABLE || status === CONNECTION_STATUS.READY) {
      this.socket.emit(SOCKET_EMITTER.START_VIDEO, data);
    } else {
      onError({ status: CONNECTION_STATUS.NOT_CONNECTED, message: CONNECTION_MESSAGE.NOT_CONNECTED });
    }
  };

  componentDidMount() {
    const { onStatusChange, onError } = this.props;

    this.socket.on(SOCKET_LISTENERS.CONNECT, () => {
      if (this.socket.connected) {
        this.socketListeners().listen(this.socket);
        onStatusChange({ status: CONNECTION_STATUS.CHECKING });
        this.setState({ isConnected: true, status: CONNECTION_STATUS.CHECKING });
      } else {
        onStatusChange({ status: CONNECTION_STATUS.NOT_CONNECTED });
        onError({ status: CONNECTION_STATUS.NOT_CONNECTED, message: CONNECTION_MESSAGE.NOT_CONNECTED });
      }
    });
  }

  getJitterTIme = async (socket) => {
    const JITTER_TIME = jitterTime(1 / 60, 0.2);
    let pings = [];
    let i = 0;
    await new Promise((resolve) => {
      let start;
      let interval;
      socket.on(SOCKET_LISTENERS.PONG, () => {
        pings.push(Date.now() - start);
        console.log('pong');
        if (i++ === 3) {
          resolve();
          clearInterval(interval);
        }
      });
      interval = setInterval(() => {
        start = Date.now();
        socket.emit(SOCKET_EMITTER.PING);
      }, JITTER_TIME);
    });
    const averagePing = pings.reduce((total, val) => total + val, 0) / pings.length;
    console.log('Average Ping', averagePing);
    let total = 0;
    pings.forEach((item) => {
      total += Math.abs(item - averagePing);
    });
    console.log('Jitter time :', total / pings.length);
    return total / pings.length <= 20;
  };

  socketListeners = () => {
    const { onRecording, onStatusChange, onResult, onError, debugMode = false, tkycToken = null } = this.props;
    console.log('Mode Debug : ', debugMode);
    return {
      listen: async (socket) => {
        const resultJitter = await this.getJitterTIme(socket);
        console.log(resultJitter);
        if (debugMode || resultJitter) {
          this.setState({ status: CONNECTION_STATUS.READY });
          onStatusChange({ status: CONNECTION_STATUS.READY });
        } else {
          this.setState({ status: CONNECTION_STATUS.CONNECTION_UNSTABLE });
          onStatusChange({ status: CONNECTION_STATUS.CONNECTION_UNSTABLE });
          onError({ status: CONNECTION_STATUS.CONNECTION_UNSTABLE, message: CONNECTION_MESSAGE.CONNECTION_UNSTABLE });
        }
        socket.on(SOCKET_LISTENERS.VIDEO_RECORDING, async (data) => {
          // console.log('callQueued', data);
          onStatusChange({ status: CONNECTION_STATUS.RECORDING });
          onRecording(data);
          this.setState((prevState) => ({
            ...prevState,
            status: CONNECTION_STATUS.RECORDING,
            videoData: { ...data },
          }));
          const { connection: remotePeerConnection } = data;
          const localPeerConnection = this.recordingConnection.createConnection({
            id: tkycToken ? tkycToken : this.state.kycToken,
          });

          const localStream = await window.navigator.mediaDevices.getUserMedia(UserMediaConfig);
          this.recordingConnection.addStream(localStream);
          this.video.srcObject = localStream;
          this.video.setAttribute('playsinline', true);
          this.video.setAttribute('webkit-playsinline', true);

          EventManager.on('WEBRTC_CONNECTED', () => {
            console.log('WEBRTC_CONNECTED');
            setTimeout(() => {
              console.log('Stop Call');
              this.stopCall();
            }, 7000);
          });

          const { close: localPeerConnectionOnClose } = localPeerConnection;
          localPeerConnection.close = function () {
            // this.video.srcObject = null;
            localStream.length && localStream.forEach((track) => track.stop());
            return localPeerConnectionOnClose.apply(this, arguments);
          };

          const updatedAnswer = await this.recordingConnection.answer(remotePeerConnection);
          const callAnwerData = {
            localConnection: updatedAnswer.toJSON(),
            callId: tkycToken ? tkycToken : this.state.kycToken,
          };
          socket.emit(SOCKET_EMITTER.CALL_ANSWER, JSON.stringify(callAnwerData));
        });

        // When client received liveness data
        socket.on(SOCKET_LISTENERS.RECEIVED_DATA, (data) => {
          onStatusChange({ status: CONNECTION_STATUS.DONE });
          onResult({ kycToken: this.state.kycToken, ...data });
          this.setState((prevState) => ({
            ...prevState,
            status: CONNECTION_STATUS.DONE,
          }));
        });
      },
    };
  };

  stopCall = () => {
    const { onStatusChange, tkycToken = null } = this.props;

    onStatusChange({ status: CONNECTION_STATUS.GENERATING_DATA });
    this.setState({ status: CONNECTION_STATUS.GENERATING_DATA });
    this.socket.emit(SOCKET_EMITTER.CANCEL_VIDEO, tkycToken ? tkycToken : this.state.kycToken);
    this.recordingConnection.close();
  };

  startCall = () => {};
  render() {
    const {
      onRecording,
      onResult,
      onStatusChange,
      onStartCall,
      onStopCall,
      onError,
      initialData,
      mediaConfig,
      mirrored,
      style = {},
      ...rest
    } = this.props;
    const { status } = this.state;

    const videoStyle = mirrored ? { ...style, transform: `${style.transform || ''} scaleX(-1)` } : style;
    if (status === CONNECTION_STATUS.GENERATING_DATA) return null;
    return <video muted autoPlay ref={(videoref) => (this.video = videoref)} style={videoStyle} {...rest}></video>;
  }
}

Video.propTypes = {
  onRecording: PropTypes.func.isRequired,
  onResult: PropTypes.func.isRequired,
  onStatusChange: PropTypes.func.isRequired,
  onStartCall: PropTypes.func.isRequired,
  onStopCall: PropTypes.func.isRequired,
  onError: PropTypes.func.isRequired,
  mirrored: PropTypes.bool,
  initialData: PropTypes.object,
  mediaConfig: PropTypes.object,
};

Video.defaultProps = {
  onRecording: (data) => {},
  onResult: (data) => {},
  onStatusChange: (data) => {},
  onStartCall: () => {},
  onStopCall: (data) => {},
  onError: (data) => {},
  mirrored: true,
  initialData: {
    data: { name: 'person' },
  },
};

export default Video;

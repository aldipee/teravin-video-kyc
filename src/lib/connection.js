import { v4 as uuid } from 'uuid';
import EventManager from './eventManager';
import Logger from './logger';

export default class Connection {
  constructor(socket) {
    this.localPeerConnection = null;
    this.stream = null;
    this.options = {};
    this.connectionTimer = null;
    this.reconnectionTimer = null;
    this.id = null;
    this.socket = socket;
    this.level = null;
  }

  createConnection(options = {}) {
    options = {
      TIME_TO_CONNECTED: 15000,
      TIME_TO_HOST_CANDIDATES: 30000,
      TIME_TO_RECONNECTED: 20000,
      beforeOffer() {},
      ...options,
    };
    this.options = options;

    if (!options.id || options.id === '') {
      options.id = uuid();
    }
    this.id = options.id;
    this.level = options.level;

    if (typeof options.beforeOffer !== 'function') {
      throw new TypeError('beforeOffer should be function!');
    }

    const localPeerConnection = (this.localPeerConnection = new RTCPeerConnection({
      iceServers: [
        {
          urls: 'stun:13.250.40.223:3478',
          username: 'teravinmeong',
          credential: 'meong123P455w02d',
        },
        {
          urls: 'turn:13.250.40.223:3478',
          username: 'teravinmeong',
          credential: 'meong123P455w02d',
        },
      ],
      iceTransportPolicy: 'all',
    }));

    options.beforeOffer(localPeerConnection);

    this.connectionTimer = setTimeout(() => {
      if (
        localPeerConnection.iceConnectionState !== 'connected' &&
        localPeerConnection.iceConnectionState !== 'completed'
      ) {
        Logger.error(`${this.id} TIMEOUT due to connection timeout`);
        this.close();
      }
    }, options.TIME_TO_CONNECTED);

    this.reconnectionTimer = null;

    localPeerConnection.oniceconnectionstatechange = () => {
      if (
        localPeerConnection.iceConnectionState === 'connected' ||
        localPeerConnection.iceConnectionState === 'completed'
      ) {
        if (this.connectionTimer) {
          clearTimeout(this.connectionTimer);
          this.connectionTimer = null;
        }
        EventManager.fire('WEBRTC_CONNECTED');
        clearTimeout(this.reconnectionTimer);
        this.reconnectionTimer = null;
      } else if (
        localPeerConnection.iceConnectionState === 'disconnected' ||
        localPeerConnection.iceConnectionState === 'failed'
      ) {
        if (!this.connectionTimer && !this.reconnectionTimer) {
          this.reconnectionTimer = setTimeout(() => {
            Logger.error(
              `${this.id} TIMEOUT due to re-connection timeout. [STS ${localPeerConnection.iceConnectionState}]`
            );
            this.close();
          }, options.TIME_TO_CONNECTED);
        }
      }
    };

    localPeerConnection.onicecandidate = (event) => {
      if (event.candidate) {
        this.socket.emit('candidate', JSON.stringify({ candidate: event.candidate, procInstId: this.id }));
      }
    };

    this.socket.on('disconnect', () => {
      console.log('Connected to Socket');
    });

    this.socket.on('candidate', async (data) => {
      data = JSON.parse(data);
      console.log('CANDIDATE', data.candidate);
      try {
        data.candidate.usernameFragment = null;
        await this.localPeerConnection
          .addIceCandidate(new RTCIceCandidate(data.candidate))
          .catch((e) => console.warn(e));
      } catch (e) {
        console.warn(e);
      }
    });

    return localPeerConnection;
  }

  addStream(stream) {
    this.stream = stream;
    // stream
    //   .getTracks()
    //   .forEach((track) => this.localPeerConnection.addTrack(track, stream));

    stream.getTracks().forEach((track) => this.localPeerConnection.addTrack(track, stream));
  }

  async offer() {
    if (this.localPeerConnection == null) {
      return;
    }

    Logger.log(`${this.id} is offering...`);

    const { localPeerConnection } = this;

    const offer = localPeerConnection.createOffer();
    await localPeerConnection.setLocalDescription(offer);

    if (localPeerConnection.iceGatheringState === 'complete') {
      return;
    }

    const { TIME_TO_HOST_CANDIDATES: timeToHostCandidates } = this.options;
    console.log(timeToHostCandidates);

    const deferred = {};
    deferred.promise = new Promise((resolve, reject) => {
      deferred.resolve = resolve;
      deferred.reject = reject;
    });

    const timeout = setTimeout(() => {
      localPeerConnection.removeEventListener('icecandidate', onIceCandidate);
      Logger.error(`${this.id} Timed out waiting for host candidates`);
      deferred.reject(new Error(`${this.id} Timed out waiting for host candidates`));
    }, timeToHostCandidates);

    function onIceCandidate({ candidate }) {
      if (!candidate) {
        clearTimeout(timeout);
        localPeerConnection.removeEventListener('icecandidate', onIceCandidate);
        deferred.resolve();
      }
    }

    localPeerConnection.addEventListener('icecandidate', onIceCandidate);

    await deferred.promise;
  }

  handleAnswer(remotePeerConnection) {
    Logger.log(`${this.id} is handling answer...`);
    this.localPeerConnection.setRemoteDescription(remotePeerConnection);
  }

  async answer(remotePeerConnection) {
    if (this.localPeerConnection == null) {
      return;
    }

    Logger.log(`${this.id} is answering...`);
    console.log(remotePeerConnection);

    this.localPeerConnection.setRemoteDescription(remotePeerConnection);
    const originalAnswer = await this.localPeerConnection.createAnswer();
    const updatedAnswer = new RTCSessionDescription({
      type: 'answer',
      sdp: originalAnswer.sdp,
    });
    await this.localPeerConnection.setLocalDescription(updatedAnswer);
    return updatedAnswer;
  }

  close() {
    const { localPeerConnection } = this;
    const onIceConnectionStateChange = localPeerConnection.oniceconnectionstatechange;

    localPeerConnection.removeEventListener('iceconnectionstatechange', onIceConnectionStateChange);
    if (this.connectionTimer) {
      clearTimeout(this.connectionTimer);
      this.connectionTimer = null;
    }
    if (this.reconnectionTimer) {
      clearTimeout(this.reconnectionTimer);
      this.reconnectionTimer = null;
    }
    localPeerConnection.close();

    Logger.log(`${this.id} is closed`);
    this.id = null;
    this.stream = null;
    this.localPeerConnection = null;
    this.options = null;
    this.socket.removeAllListeners('candidate');
  }
}

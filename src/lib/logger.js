// eslint-disable-next-line import/no-anonymous-default-export
export default {
    log(str) {
      console.log(str);
    },
    error(str) {
      console.error(str);
    },
    warn(str) {
      console.warn(str);
    },
  };
  
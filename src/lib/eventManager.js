'use strict';
function EventManager() {
  const id = Date.now();
  let namespace = id;

  const events = {};
  events[id] = {};

  // Listen for an event on current namespace and eventually switch to root namespace.
  // @param eventName : string Name of the event
  // @param handler : action   Callback called when the event occurs
  // @return this
  EventManager.prototype.on = function (eventName, handler) {
    const e = events[namespace][eventName];

    if (typeof e === 'undefined') {
      events[namespace][eventName] = [];
    }

    events[namespace][eventName].push(handler);

    return this.useBaseNamespace();
  };

  // Fire an event on current namespace and eventually switch to root namespace.
  // @param eventName : string Occuring event name
  // @param data : object      Data that will be passwed through event handlers
  // @return this
  EventManager.prototype.fire = function (eventName, data) {
    const n = events[namespace];

    if (typeof n === 'undefined') {
      return this.useBaseNamespace();
    }

    const e = n[eventName];

    if (typeof e === 'undefined') {
      return this.useBaseNamespace();
    }

    for (let action of e) {
      action(data);
    }

    return this.useBaseNamespace();
  };

  // Change/switch namespace.
  // @param namespaceName : string Name of the namespace
  // @return this
  EventManager.prototype.of = function (namespaceName) {
    if (namespace !== id) {
      namespace += `/${namespaceName}`;
    } else {
      namespace = namespaceName;
    }

    if (typeof events[namespace] === 'undefined') {
      events[namespace] = {};
    }

    return this;
  };

  // Change/switch namespace to root
  // @return this
  EventManager.prototype.useBaseNamespace = function () {
    namespace = id;

    return this;
  };

  // Clear all events and subnamespaces on current namespace
  // and eventually switch to root namespace.
  // @return this
  EventManager.prototype.clearEvents = function () {
    events[namespace] = {};

    return this.useBaseNamespace();
  };

  // Clear all events and subnamespaces regardless the namespace
  // and eventually switch to root namespace.
  // @return this
  EventManager.prototype.clearAllEvents = function () {
    for (let n of Object.keys(events)) {
      events[n] = {};
    }

    return this.useBaseNamespace();
  };

  // Get all events. For debugging.
  // WARN: Remove this if you want to keep events private.
  EventManager.prototype.getEvents = function () {
    return events;
  };
}

export default new EventManager();

const UserMediaConfig = {
  audio: true,
  video: {
    frameRate: { ideal: 10, max: 15 },
    width: {
      max: 640,
      ideal: 640,
    },
    height: {
      max: 800,
      ideal: 800,
    },
  },
};

export { UserMediaConfig };

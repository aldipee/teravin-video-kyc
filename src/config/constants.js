const CONNECTION_STATUS = {
  CHECKING: 'CHECKING_INTERNET_CONNECTION',
  INITIAL: 'INITIAL_CONNECT',
  RECORDING: 'RECORDING',
  OFFLINE: 'OFFLINE',
  GENERATING_DATA: 'GENERATING_DATA',
  DONE: 'DONE',
  READY: 'READY',
  NOT_CONNECTED: 'NOT_CONNECTED',
  CONNECTION_UNSTABLE: 'CONNECTION_UNSTABLE',
};

const CONNECTION_MESSAGE = {
  NOT_CONNECTED: 'Cannot Connect to WS Server',
  CONNECTION_UNSTABLE: 'Your internet connection is unstable, please make sure you have good connection',
};

const SOCKET_LISTENERS = {
  CONNECT: 'connect',
  PONG: 'testpong',
  VIDEO_RECORDING: 'callQueued',
  RECEIVED_DATA: 'livenessScored',
  GET_KYC_TOKEN: 'init'
};

const SOCKET_EMITTER = {
  PING: 'testping',
  START_VIDEO: 'queueCall',
  CANCEL_VIDEO: 'cancelQueuedCall',
  GET_KYC_TOKEN: 'init',
  CALL_ANSWER: 'queueCall_ANSWER'
};

export { CONNECTION_STATUS, SOCKET_EMITTER, SOCKET_LISTENERS, CONNECTION_MESSAGE };

import React from 'react';
import io from 'socket.io-client';
import PropTypes from 'prop-types';
import { v4 } from 'uuid';
import jitterTime from 'jitter-time';

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);

    if (enumerableOnly) {
      symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
    }

    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread2(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(Object(source), true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(Object(source)).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
  try {
    var info = gen[key](arg);
    var value = info.value;
  } catch (error) {
    reject(error);
    return;
  }

  if (info.done) {
    resolve(value);
  } else {
    Promise.resolve(value).then(_next, _throw);
  }
}

function _asyncToGenerator(fn) {
  return function () {
    var self = this,
        args = arguments;
    return new Promise(function (resolve, reject) {
      var gen = fn.apply(self, args);

      function _next(value) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
      }

      function _throw(err) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
      }

      _next(undefined);
    });
  };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

function _isNativeReflectConstruct() {
  if (typeof Reflect === "undefined" || !Reflect.construct) return false;
  if (Reflect.construct.sham) return false;
  if (typeof Proxy === "function") return true;

  try {
    Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {}));
    return true;
  } catch (e) {
    return false;
  }
}

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

function _objectWithoutProperties(source, excluded) {
  if (source == null) return {};

  var target = _objectWithoutPropertiesLoose(source, excluded);

  var key, i;

  if (Object.getOwnPropertySymbols) {
    var sourceSymbolKeys = Object.getOwnPropertySymbols(source);

    for (i = 0; i < sourceSymbolKeys.length; i++) {
      key = sourceSymbolKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
      target[key] = source[key];
    }
  }

  return target;
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _possibleConstructorReturn(self, call) {
  if (call && (typeof call === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _createSuper(Derived) {
  var hasNativeReflectConstruct = _isNativeReflectConstruct();

  return function _createSuperInternal() {
    var Super = _getPrototypeOf(Derived),
        result;

    if (hasNativeReflectConstruct) {
      var NewTarget = _getPrototypeOf(this).constructor;

      result = Reflect.construct(Super, arguments, NewTarget);
    } else {
      result = Super.apply(this, arguments);
    }

    return _possibleConstructorReturn(this, result);
  };
}

function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return _arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
}

function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;

  for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];

  return arr2;
}

function _createForOfIteratorHelper(o, allowArrayLike) {
  var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"];

  if (!it) {
    if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") {
      if (it) o = it;
      var i = 0;

      var F = function () {};

      return {
        s: F,
        n: function () {
          if (i >= o.length) return {
            done: true
          };
          return {
            done: false,
            value: o[i++]
          };
        },
        e: function (e) {
          throw e;
        },
        f: F
      };
    }

    throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
  }

  var normalCompletion = true,
      didErr = false,
      err;
  return {
    s: function () {
      it = it.call(o);
    },
    n: function () {
      var step = it.next();
      normalCompletion = step.done;
      return step;
    },
    e: function (e) {
      didErr = true;
      err = e;
    },
    f: function () {
      try {
        if (!normalCompletion && it.return != null) it.return();
      } finally {
        if (didErr) throw err;
      }
    }
  };
}

function EventManager() {
  var id = Date.now();
  var namespace = id;
  var events = {};
  events[id] = {}; // Listen for an event on current namespace and eventually switch to root namespace.
  // @param eventName : string Name of the event
  // @param handler : action   Callback called when the event occurs
  // @return this

  EventManager.prototype.on = function (eventName, handler) {
    var e = events[namespace][eventName];

    if (typeof e === 'undefined') {
      events[namespace][eventName] = [];
    }

    events[namespace][eventName].push(handler);
    return this.useBaseNamespace();
  }; // Fire an event on current namespace and eventually switch to root namespace.
  // @param eventName : string Occuring event name
  // @param data : object      Data that will be passwed through event handlers
  // @return this


  EventManager.prototype.fire = function (eventName, data) {
    var n = events[namespace];

    if (typeof n === 'undefined') {
      return this.useBaseNamespace();
    }

    var e = n[eventName];

    if (typeof e === 'undefined') {
      return this.useBaseNamespace();
    }

    var _iterator = _createForOfIteratorHelper(e),
        _step;

    try {
      for (_iterator.s(); !(_step = _iterator.n()).done;) {
        var action = _step.value;
        action(data);
      }
    } catch (err) {
      _iterator.e(err);
    } finally {
      _iterator.f();
    }

    return this.useBaseNamespace();
  }; // Change/switch namespace.
  // @param namespaceName : string Name of the namespace
  // @return this


  EventManager.prototype.of = function (namespaceName) {
    if (namespace !== id) {
      namespace += "/".concat(namespaceName);
    } else {
      namespace = namespaceName;
    }

    if (typeof events[namespace] === 'undefined') {
      events[namespace] = {};
    }

    return this;
  }; // Change/switch namespace to root
  // @return this


  EventManager.prototype.useBaseNamespace = function () {
    namespace = id;
    return this;
  }; // Clear all events and subnamespaces on current namespace
  // and eventually switch to root namespace.
  // @return this


  EventManager.prototype.clearEvents = function () {
    events[namespace] = {};
    return this.useBaseNamespace();
  }; // Clear all events and subnamespaces regardless the namespace
  // and eventually switch to root namespace.
  // @return this


  EventManager.prototype.clearAllEvents = function () {
    for (var _i = 0, _Object$keys = Object.keys(events); _i < _Object$keys.length; _i++) {
      var n = _Object$keys[_i];
      events[n] = {};
    }

    return this.useBaseNamespace();
  }; // Get all events. For debugging.
  // WARN: Remove this if you want to keep events private.


  EventManager.prototype.getEvents = function () {
    return events;
  };
}

var EventManager$1 = new EventManager();

// eslint-disable-next-line import/no-anonymous-default-export
var Logger = {
  log: function log(str) {
    console.log(str);
  },
  error: function error(str) {
    console.error(str);
  },
  warn: function warn(str) {
    console.warn(str);
  }
};

var Connection = /*#__PURE__*/function () {
  function Connection(socket) {
    _classCallCheck(this, Connection);

    this.localPeerConnection = null;
    this.stream = null;
    this.options = {};
    this.connectionTimer = null;
    this.reconnectionTimer = null;
    this.id = null;
    this.socket = socket;
    this.level = null;
  }

  _createClass(Connection, [{
    key: "createConnection",
    value: function createConnection() {
      var _this = this;

      var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      options = _objectSpread2({
        TIME_TO_CONNECTED: 15000,
        TIME_TO_HOST_CANDIDATES: 30000,
        TIME_TO_RECONNECTED: 20000,
        beforeOffer: function beforeOffer() {}
      }, options);
      this.options = options;

      if (!options.id || options.id === '') {
        options.id = v4();
      }

      this.id = options.id;
      this.level = options.level;

      if (typeof options.beforeOffer !== 'function') {
        throw new TypeError('beforeOffer should be function!');
      }

      var localPeerConnection = this.localPeerConnection = new RTCPeerConnection({
        iceServers: [{
          urls: 'stun:13.250.40.223:3478',
          username: 'teravinmeong',
          credential: 'meong123P455w02d'
        }, {
          urls: 'turn:13.250.40.223:3478',
          username: 'teravinmeong',
          credential: 'meong123P455w02d'
        }],
        iceTransportPolicy: 'all'
      });
      options.beforeOffer(localPeerConnection);
      this.connectionTimer = setTimeout(function () {
        if (localPeerConnection.iceConnectionState !== 'connected' && localPeerConnection.iceConnectionState !== 'completed') {
          Logger.error("".concat(_this.id, " TIMEOUT due to connection timeout"));

          _this.close();
        }
      }, options.TIME_TO_CONNECTED);
      this.reconnectionTimer = null;

      localPeerConnection.oniceconnectionstatechange = function () {
        if (localPeerConnection.iceConnectionState === 'connected' || localPeerConnection.iceConnectionState === 'completed') {
          if (_this.connectionTimer) {
            clearTimeout(_this.connectionTimer);
            _this.connectionTimer = null;
          }

          EventManager$1.fire('WEBRTC_CONNECTED');
          clearTimeout(_this.reconnectionTimer);
          _this.reconnectionTimer = null;
        } else if (localPeerConnection.iceConnectionState === 'disconnected' || localPeerConnection.iceConnectionState === 'failed') {
          if (!_this.connectionTimer && !_this.reconnectionTimer) {
            _this.reconnectionTimer = setTimeout(function () {
              Logger.error("".concat(_this.id, " TIMEOUT due to re-connection timeout. [STS ").concat(localPeerConnection.iceConnectionState, "]"));

              _this.close();
            }, options.TIME_TO_CONNECTED);
          }
        }
      };

      localPeerConnection.onicecandidate = function (event) {
        if (event.candidate) {
          _this.socket.emit('candidate', JSON.stringify({
            candidate: event.candidate,
            procInstId: _this.id
          }));
        }
      };

      this.socket.on('disconnect', function () {
        console.log('Connected to Socket');
      });
      this.socket.on('candidate', /*#__PURE__*/function () {
        var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(data) {
          return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  data = JSON.parse(data);
                  console.log('CANDIDATE', data.candidate);
                  _context.prev = 2;
                  data.candidate.usernameFragment = null;
                  _context.next = 6;
                  return _this.localPeerConnection.addIceCandidate(new RTCIceCandidate(data.candidate)).catch(function (e) {
                    return console.warn(e);
                  });

                case 6:
                  _context.next = 11;
                  break;

                case 8:
                  _context.prev = 8;
                  _context.t0 = _context["catch"](2);
                  console.warn(_context.t0);

                case 11:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee, null, [[2, 8]]);
        }));

        return function (_x) {
          return _ref.apply(this, arguments);
        };
      }());
      return localPeerConnection;
    }
  }, {
    key: "addStream",
    value: function addStream(stream) {
      var _this2 = this;

      this.stream = stream; // stream
      //   .getTracks()
      //   .forEach((track) => this.localPeerConnection.addTrack(track, stream));

      stream.getTracks().forEach(function (track) {
        return _this2.localPeerConnection.addTrack(track, stream);
      });
    }
  }, {
    key: "offer",
    value: function () {
      var _offer = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
        var _this3 = this;

        var localPeerConnection, offer, timeToHostCandidates, deferred, timeout, onIceCandidate;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                onIceCandidate = function _onIceCandidate(_ref2) {
                  var candidate = _ref2.candidate;

                  if (!candidate) {
                    clearTimeout(timeout);
                    localPeerConnection.removeEventListener('icecandidate', onIceCandidate);
                    deferred.resolve();
                  }
                };

                if (!(this.localPeerConnection == null)) {
                  _context2.next = 3;
                  break;
                }

                return _context2.abrupt("return");

              case 3:
                Logger.log("".concat(this.id, " is offering..."));
                localPeerConnection = this.localPeerConnection;
                offer = localPeerConnection.createOffer();
                _context2.next = 8;
                return localPeerConnection.setLocalDescription(offer);

              case 8:
                if (!(localPeerConnection.iceGatheringState === 'complete')) {
                  _context2.next = 10;
                  break;
                }

                return _context2.abrupt("return");

              case 10:
                timeToHostCandidates = this.options.TIME_TO_HOST_CANDIDATES;
                console.log(timeToHostCandidates);
                deferred = {};
                deferred.promise = new Promise(function (resolve, reject) {
                  deferred.resolve = resolve;
                  deferred.reject = reject;
                });
                timeout = setTimeout(function () {
                  localPeerConnection.removeEventListener('icecandidate', onIceCandidate);
                  Logger.error("".concat(_this3.id, " Timed out waiting for host candidates"));
                  deferred.reject(new Error("".concat(_this3.id, " Timed out waiting for host candidates")));
                }, timeToHostCandidates);
                localPeerConnection.addEventListener('icecandidate', onIceCandidate);
                _context2.next = 18;
                return deferred.promise;

              case 18:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function offer() {
        return _offer.apply(this, arguments);
      }

      return offer;
    }()
  }, {
    key: "handleAnswer",
    value: function handleAnswer(remotePeerConnection) {
      Logger.log("".concat(this.id, " is handling answer..."));
      this.localPeerConnection.setRemoteDescription(remotePeerConnection);
    }
  }, {
    key: "answer",
    value: function () {
      var _answer = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(remotePeerConnection) {
        var originalAnswer, updatedAnswer;
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                if (!(this.localPeerConnection == null)) {
                  _context3.next = 2;
                  break;
                }

                return _context3.abrupt("return");

              case 2:
                Logger.log("".concat(this.id, " is answering..."));
                console.log(remotePeerConnection);
                this.localPeerConnection.setRemoteDescription(remotePeerConnection);
                _context3.next = 7;
                return this.localPeerConnection.createAnswer();

              case 7:
                originalAnswer = _context3.sent;
                updatedAnswer = new RTCSessionDescription({
                  type: 'answer',
                  sdp: originalAnswer.sdp
                });
                _context3.next = 11;
                return this.localPeerConnection.setLocalDescription(updatedAnswer);

              case 11:
                return _context3.abrupt("return", updatedAnswer);

              case 12:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function answer(_x2) {
        return _answer.apply(this, arguments);
      }

      return answer;
    }()
  }, {
    key: "close",
    value: function close() {
      var localPeerConnection = this.localPeerConnection;
      var onIceConnectionStateChange = localPeerConnection.oniceconnectionstatechange;
      localPeerConnection.removeEventListener('iceconnectionstatechange', onIceConnectionStateChange);

      if (this.connectionTimer) {
        clearTimeout(this.connectionTimer);
        this.connectionTimer = null;
      }

      if (this.reconnectionTimer) {
        clearTimeout(this.reconnectionTimer);
        this.reconnectionTimer = null;
      }

      localPeerConnection.close();
      Logger.log("".concat(this.id, " is closed"));
      this.id = null;
      this.stream = null;
      this.localPeerConnection = null;
      this.options = null;
      this.socket.removeAllListeners('candidate');
    }
  }]);

  return Connection;
}();

var CONNECTION_STATUS = {
  CHECKING: 'CHECKING_INTERNET_CONNECTION',
  INITIAL: 'INITIAL_CONNECT',
  RECORDING: 'RECORDING',
  OFFLINE: 'OFFLINE',
  GENERATING_DATA: 'GENERATING_DATA',
  DONE: 'DONE',
  READY: 'READY',
  NOT_CONNECTED: 'NOT_CONNECTED',
  CONNECTION_UNSTABLE: 'CONNECTION_UNSTABLE'
};
var CONNECTION_MESSAGE = {
  NOT_CONNECTED: 'Cannot Connect to WS Server',
  CONNECTION_UNSTABLE: 'Your internet connection is unstable, please make sure you have good connection'
};
var SOCKET_LISTENERS = {
  CONNECT: 'connect',
  PONG: 'testpong',
  VIDEO_RECORDING: 'callQueued',
  RECEIVED_DATA: 'livenessScored',
  GET_KYC_TOKEN: 'init'
};
var SOCKET_EMITTER = {
  PING: 'testping',
  START_VIDEO: 'queueCall',
  CANCEL_VIDEO: 'cancelQueuedCall',
  GET_KYC_TOKEN: 'init',
  CALL_ANSWER: 'queueCall_ANSWER'
};

var UserMediaConfig = {
  audio: true,
  video: {
    frameRate: {
      ideal: 10,
      max: 15
    },
    width: {
      max: 640,
      ideal: 640
    },
    height: {
      max: 800,
      ideal: 800
    }
  }
};

var _excluded = ["onRecording", "onResult", "onStatusChange", "onStartCall", "onStopCall", "onError", "initialData", "mediaConfig", "mirrored", "style"];

var Video = /*#__PURE__*/function (_React$Component) {
  _inherits(Video, _React$Component);

  var _super = _createSuper(Video);

  function Video(props) {
    var _this;

    _classCallCheck(this, Video);

    _this = _super.call(this, props);

    _defineProperty(_assertThisInitialized(_this), "Init", function () {
      var onStatusChange = _this.props.onStatusChange;

      _this.socket.emit(SOCKET_EMITTER.GET_KYC_TOKEN);

      onStatusChange({
        status: CONNECTION_STATUS.INITIAL
      });

      _this.setState({
        status: CONNECTION_STATUS.INITIAL
      });

      return new Promise(function (resolve, reject) {
        _this.socket.on(SOCKET_LISTENERS.GET_KYC_TOKEN, function (data) {
          _this.setState({
            kycToken: data.callId
          });

          resolve({
            kycToken: _this.state.kycToken
          });
        });
      });
    });

    _defineProperty(_assertThisInitialized(_this), "StartLiveness", function () {
      var _this$props = _this.props,
          initialData = _this$props.initialData,
          onError = _this$props.onError,
          _this$props$tkycToken = _this$props.tkycToken,
          tkycToken = _this$props$tkycToken === void 0 ? null : _this$props$tkycToken;
      var status = _this.state.status;

      var data = _objectSpread2({
        callId: tkycToken ? tkycToken : _this.state.kycToken
      }, initialData);

      if (status !== CONNECTION_STATUS.CONNECTION_UNSTABLE || status === CONNECTION_STATUS.READY) {
        _this.socket.emit(SOCKET_EMITTER.START_VIDEO, data);
      } else {
        onError({
          status: CONNECTION_STATUS.NOT_CONNECTED,
          message: CONNECTION_MESSAGE.NOT_CONNECTED
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this), "getJitterTIme", /*#__PURE__*/function () {
      var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(socket) {
        var JITTER_TIME, pings, i, averagePing, total;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                JITTER_TIME = jitterTime(1 / 60, 0.2);
                pings = [];
                i = 0;
                _context.next = 5;
                return new Promise(function (resolve) {
                  var start;
                  var interval;
                  socket.on(SOCKET_LISTENERS.PONG, function () {
                    pings.push(Date.now() - start);
                    console.log('pong');

                    if (i++ === 3) {
                      resolve();
                      clearInterval(interval);
                    }
                  });
                  interval = setInterval(function () {
                    start = Date.now();
                    socket.emit(SOCKET_EMITTER.PING);
                  }, JITTER_TIME);
                });

              case 5:
                averagePing = pings.reduce(function (total, val) {
                  return total + val;
                }, 0) / pings.length;
                console.log('Average Ping', averagePing);
                total = 0;
                pings.forEach(function (item) {
                  total += Math.abs(item - averagePing);
                });
                console.log('Jitter time :', total / pings.length);
                return _context.abrupt("return", total / pings.length <= 20);

              case 11:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }());

    _defineProperty(_assertThisInitialized(_this), "socketListeners", function () {
      var _this$props2 = _this.props,
          onRecording = _this$props2.onRecording,
          onStatusChange = _this$props2.onStatusChange,
          onResult = _this$props2.onResult,
          onError = _this$props2.onError,
          _this$props2$debugMod = _this$props2.debugMode,
          debugMode = _this$props2$debugMod === void 0 ? false : _this$props2$debugMod,
          _this$props2$tkycToke = _this$props2.tkycToken,
          tkycToken = _this$props2$tkycToke === void 0 ? null : _this$props2$tkycToke;
      console.log('Mode Debug : ', debugMode);
      return {
        listen: function () {
          var _listen = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(socket) {
            var resultJitter;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    _context3.next = 2;
                    return _this.getJitterTIme(socket);

                  case 2:
                    resultJitter = _context3.sent;
                    console.log(resultJitter);

                    if (debugMode || resultJitter) {
                      _this.setState({
                        status: CONNECTION_STATUS.READY
                      });

                      onStatusChange({
                        status: CONNECTION_STATUS.READY
                      });
                    } else {
                      _this.setState({
                        status: CONNECTION_STATUS.CONNECTION_UNSTABLE
                      });

                      onStatusChange({
                        status: CONNECTION_STATUS.CONNECTION_UNSTABLE
                      });
                      onError({
                        status: CONNECTION_STATUS.CONNECTION_UNSTABLE,
                        message: CONNECTION_MESSAGE.CONNECTION_UNSTABLE
                      });
                    }

                    socket.on(SOCKET_LISTENERS.VIDEO_RECORDING, /*#__PURE__*/function () {
                      var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(data) {
                        var remotePeerConnection, localPeerConnection, localStream, localPeerConnectionOnClose, updatedAnswer, callAnwerData;
                        return regeneratorRuntime.wrap(function _callee2$(_context2) {
                          while (1) {
                            switch (_context2.prev = _context2.next) {
                              case 0:
                                // console.log('callQueued', data);
                                onStatusChange({
                                  status: CONNECTION_STATUS.RECORDING
                                });
                                onRecording(data);

                                _this.setState(function (prevState) {
                                  return _objectSpread2(_objectSpread2({}, prevState), {}, {
                                    status: CONNECTION_STATUS.RECORDING,
                                    videoData: _objectSpread2({}, data)
                                  });
                                });

                                remotePeerConnection = data.connection;
                                localPeerConnection = _this.recordingConnection.createConnection({
                                  id: tkycToken ? tkycToken : _this.state.kycToken
                                });
                                _context2.next = 7;
                                return window.navigator.mediaDevices.getUserMedia(UserMediaConfig);

                              case 7:
                                localStream = _context2.sent;

                                _this.recordingConnection.addStream(localStream);

                                _this.video.srcObject = localStream;

                                _this.video.setAttribute('playsinline', true);

                                _this.video.setAttribute('webkit-playsinline', true);

                                EventManager$1.on('WEBRTC_CONNECTED', function () {
                                  console.log('WEBRTC_CONNECTED');
                                  setTimeout(function () {
                                    console.log('Stop Call');

                                    _this.stopCall();
                                  }, 7000);
                                });
                                localPeerConnectionOnClose = localPeerConnection.close;

                                localPeerConnection.close = function () {
                                  // this.video.srcObject = null;
                                  localStream.length && localStream.forEach(function (track) {
                                    return track.stop();
                                  });
                                  return localPeerConnectionOnClose.apply(this, arguments);
                                };

                                _context2.next = 17;
                                return _this.recordingConnection.answer(remotePeerConnection);

                              case 17:
                                updatedAnswer = _context2.sent;
                                callAnwerData = {
                                  localConnection: updatedAnswer.toJSON(),
                                  callId: tkycToken ? tkycToken : _this.state.kycToken
                                };
                                socket.emit(SOCKET_EMITTER.CALL_ANSWER, JSON.stringify(callAnwerData));

                              case 20:
                              case "end":
                                return _context2.stop();
                            }
                          }
                        }, _callee2);
                      }));

                      return function (_x3) {
                        return _ref2.apply(this, arguments);
                      };
                    }()); // When client received liveness data

                    socket.on(SOCKET_LISTENERS.RECEIVED_DATA, function (data) {
                      onStatusChange({
                        status: CONNECTION_STATUS.DONE
                      });
                      onResult(_objectSpread2({
                        kycToken: _this.state.kycToken
                      }, data));

                      _this.setState(function (prevState) {
                        return _objectSpread2(_objectSpread2({}, prevState), {}, {
                          status: CONNECTION_STATUS.DONE
                        });
                      });
                    });

                  case 7:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3);
          }));

          function listen(_x2) {
            return _listen.apply(this, arguments);
          }

          return listen;
        }()
      };
    });

    _defineProperty(_assertThisInitialized(_this), "stopCall", function () {
      var _this$props3 = _this.props,
          onStatusChange = _this$props3.onStatusChange,
          _this$props3$tkycToke = _this$props3.tkycToken,
          tkycToken = _this$props3$tkycToke === void 0 ? null : _this$props3$tkycToke;
      onStatusChange({
        status: CONNECTION_STATUS.GENERATING_DATA
      });

      _this.setState({
        status: CONNECTION_STATUS.GENERATING_DATA
      });

      _this.socket.emit(SOCKET_EMITTER.CANCEL_VIDEO, tkycToken ? tkycToken : _this.state.kycToken);

      _this.recordingConnection.close();
    });

    _defineProperty(_assertThisInitialized(_this), "startCall", function () {});

    _this.state = {
      status: CONNECTION_STATUS.OFFLINE,
      videoData: {},
      callStatus: '',
      kycToken: ''
    };
    _this.socket = io("https://liveness.colekbayar.com?room=customer", {
      transports: ['websocket']
    });
    _this.recordingConnection = new Connection(_this.socket);
    _this.peerConnection = new Connection(_this.socket);
    _this.video = null;
    return _this;
  }

  _createClass(Video, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      var _this$props4 = this.props,
          onStatusChange = _this$props4.onStatusChange,
          onError = _this$props4.onError;
      this.socket.on(SOCKET_LISTENERS.CONNECT, function () {
        if (_this2.socket.connected) {
          _this2.socketListeners().listen(_this2.socket);

          onStatusChange({
            status: CONNECTION_STATUS.CHECKING
          });

          _this2.setState({
            isConnected: true,
            status: CONNECTION_STATUS.CHECKING
          });
        } else {
          onStatusChange({
            status: CONNECTION_STATUS.NOT_CONNECTED
          });
          onError({
            status: CONNECTION_STATUS.NOT_CONNECTED,
            message: CONNECTION_MESSAGE.NOT_CONNECTED
          });
        }
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      var _this$props5 = this.props;
          _this$props5.onRecording;
          _this$props5.onResult;
          _this$props5.onStatusChange;
          _this$props5.onStartCall;
          _this$props5.onStopCall;
          _this$props5.onError;
          _this$props5.initialData;
          _this$props5.mediaConfig;
          var mirrored = _this$props5.mirrored,
          _this$props5$style = _this$props5.style,
          style = _this$props5$style === void 0 ? {} : _this$props5$style,
          rest = _objectWithoutProperties(_this$props5, _excluded);

      var status = this.state.status;
      var videoStyle = mirrored ? _objectSpread2(_objectSpread2({}, style), {}, {
        transform: "".concat(style.transform || '', " scaleX(-1)")
      }) : style;
      if (status === CONNECTION_STATUS.GENERATING_DATA) return null;
      return /*#__PURE__*/React.createElement("video", _extends({
        muted: true,
        autoPlay: true,
        ref: function ref(videoref) {
          return _this3.video = videoref;
        },
        style: videoStyle
      }, rest));
    }
  }]);

  return Video;
}(React.Component);

Video.propTypes = {
  onRecording: PropTypes.func.isRequired,
  onResult: PropTypes.func.isRequired,
  onStatusChange: PropTypes.func.isRequired,
  onStartCall: PropTypes.func.isRequired,
  onStopCall: PropTypes.func.isRequired,
  onError: PropTypes.func.isRequired,
  mirrored: PropTypes.bool,
  initialData: PropTypes.object,
  mediaConfig: PropTypes.object
};
Video.defaultProps = {
  onRecording: function onRecording(data) {},
  onResult: function onResult(data) {},
  onStatusChange: function onStatusChange(data) {},
  onStartCall: function onStartCall() {},
  onStopCall: function onStopCall(data) {},
  onError: function onError(data) {},
  mirrored: true,
  initialData: {
    data: {
      name: 'person'
    }
  }
};

export { Video };
